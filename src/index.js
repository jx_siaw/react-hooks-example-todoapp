import "./styles.css";

import React, { memo } from "react";
import ReactDOM from "react-dom";

import { useInputValue, useTodos } from "./Hooks/custom-hooks";

import Layout from "./Components/Layout";

import AddTodo from "./Components/AddTodo";
import TodoList from "./Components/TodoList";

const TodoApp = memo(props => {
  const { inputValue, changeInput, clearInput, keyInput } = useInputValue();
  const { todos, addTodo, checkTodo, removeTodo } = useTodos();

  const clearInputAndAddTodo = _ => {
    clearInput();
    addTodo(inputValue);
  };

  return (
    <Layout>
      <AddTodo
        inputValue={inputValue}
        onInputChange={changeInput}
        onButtonClick={clearInputAndAddTodo}
        onInputKeyPress={event => keyInput(event, clearInputAndAddTodo)}
      />
      <TodoList
        items={todos}
        onItemCheck={idx => checkTodo(idx)}
        onItemRemove={idx => removeTodo(idx)}
      />
    </Layout>
  );
});

ReactDOM.render(<TodoApp />, document.getElementById("root"));
